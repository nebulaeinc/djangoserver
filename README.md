# README #

Emotion Recogntion in realtime using machine learning.
The system uses GSR signals from an arduino to train and
compare against an SVM to detect whether a user may be potentially stressed

### Technologies ###

This is my final year dissertation project on emotion recognition
It received a 2.1 and used a wide array of tecnhologies including:<br/>

1) Django 

2) MongoDb

3) Mysql

4) Node.js

5) Arduino sketch

6) Javascript/Jquery/Html/CSS


### Key achievments ###

Manage to create a realtime signal processing backend for live GSR data feeds


### Additional Info ###

The dissertation is available here:
https://1drv.ms/w/s!AsbeNJzomzfLg7s1bZdnC6ik0U26sw