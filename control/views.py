#directory specific functions
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
from django.contrib.sessions.models import Session
from django.contrib.auth.decorators import login_required

from control.models import *
import json

from multiprocessing import Process, Queue

import arff


"""
Scientific imports
"""
import numpy as np
import scipy as sp
#from scipy import * as sp
from scipy import signal, misc
import matplotlib.pyplot as plt

import sys
from numpy import *
import peakutils
from peakutils.plot import plot as pplot

@csrf_exempt
def receive(request):
    try:
        test = request.POST.get('data')
        p = Rawsamples(sampleid='1', sampletxt=test, samplearff=test)
        p.save()

        raw = p.sampletxt.split(',')
        raw = map(int, raw)
        signalproc = signallib(raw, 2, 20, 1, 1200)
        #downed = signalproc.downsample()
        complete = signalproc.manager()


        #Get User from sessionid
        #session = Session.objects.get(session_key=request.POST.get('sessionid'))
        #user_id = session.get_decoded().get('_auth_user_id')
        #user = AuthUser.objects.get(id=user_id)

        return HttpResponse(complete)
    except Exception as e:
        return HttpResponseServerError(str(e))

class signallib:
    #size of the initial arrays are 3600 at 20hz
    def raw(self, raws):
        global raw
        raw = raws

    def __init__(self, Praw, downfactorx, hertz, duration, length):
        """
        PRE-PROCESSING Vars
        downfactor = how much to downsample array
        hertz = how many inputs persecond
        duration = duration of sample collection
        length = length of raw data
        """
        # raw array
        #raw(Praw)
        self.raw = Praw
        # ray numpy array
        self.nraw = np.asarray(self.raw)
        # normalised array
        self.norm = np.empty(length/downfactorx)
        # hz
        self.hz = hertz
        # the downsampled array
        self.t = np.empty(length/downfactorx)
        # length of downsampled & normalised
        self.dnlength = length/downfactorx
        #length of raw array
        self.rawlength = length
        #downsamplfactor
        self.downfactor = downfactorx
        #sample test signal
        self.sample_signal = np.empty(length/downfactorx)


    def manager(self):
        try:
            down = self.downsample()
            phasic = self.phasic()
            gen_sig = self.gen_signal()
            peaks = self.peak_detection()

            #totalpeaks = self.total_peaks(peaks)
            mean = self.mean()
            stdev = self.st_deviation()
            #NEW
            op1 = self.commit_features(20,mean,stdev)
            op2 = self.writearff()

            plt.show()
            return peaks
        except Exception as e:
            return str(e)


    def dbmanager(self):
        # add final feature set to database
        # associate final feature set with user id
        flag = "done"
        return flag

    def downsample(self):
        try:
            self.t = np.array(sp.signal.decimate(self.nraw, self.downfactor))
            # red dashes, blue squares and green triangles
            #plt.plot(np.arange(self.nraw.size),self.nraw, np.arange(self.nraw.size)/2,t)
            #x1 = np.linspace(0, self.rawlength * 2, self.rawlength)
            #x2 = np.linspace(0, 600, self.dnlength) # downsample
            #plt.plot(x1, self.nraw)
            #plt.plot(x2, self.t)


            #savisky-golay filter vs moving median average filter
            sav_gol_arr = sp.signal.savgol_filter(self.nraw, 19, 2)

            #x5 = np.linspace(0, 600, len(sav_gol_arr))
            #plt.plot(x5, sav_gol_arr)

            return len(sav_gol_arr)
        except Exception as e:
            return str(e)

    def phasic(self):
        try:
            # 20/5  = 4  [hz]/[downfactor] = 10
            # 60/2  = 15 [min]/[2sec] = 30 chunks
            # 600/30 = 16 [rawsize/downfactor] / [15 chunks] = 16 ints in each chunk of 4 secs

            #20/2 = 10
            #600/15  = 40      600/15
            # how many ints in 4 sec segment

            # phasic output
            test = np.empty(20) #16
            normitt = 0
            itt = 0
            seg = np.split(self.t, 30) #downsample = 4, * 4secs x2/ans
            segmax = len(seg)
            while (itt < segmax):
                if(itt == 0): #first
                    med1 = int(np.median(seg[itt+1]))
                    for sig in seg[itt]:
                        self.norm[normitt] = sig - med1
                        normitt += 1
                elif (itt == segmax-1): #last
                    med1 = np.median(seg[itt-1])
                    for sig in seg[itt]:
                        self.norm[normitt] = sig - med1
                        normitt += 1
                else:
                    med1 = np.median(seg[itt-1])
                    med2 = np.median(seg[itt+1])
                    avg =  (med1 + med2) / 2
                    for sig in seg[itt]:
                        self.norm[normitt] = sig - avg
                        normitt += 1
                itt += 1

            #x3 = np.linspace(0, 600, 600) #phasic
            #plt.plot(x3, self.norm)


            flag = "phasic"
            return flag
        except Exception as e:
            return str(e)

    def peak_detection(self):
        try:
            #indexes = self.peakdet(self.norm, 0.1)
            cb = self.norm

            x = np.linspace(0, 1200, 100)
            self.sample_signal = np.sin(x) + np.random.random(100) * 0.2

            indexes = peakutils.indexes(self.sample_signal, thres=0.5, min_dist=10)
            #indexes = sp.signal.find_peaks_cwt(self.sample_signal, np.arange(20,50) )
            #maxtab = sp.signal.find_peaks_cwt(self.sample_signal, np.arange(3,5,7) )

            #xpd = np.linspace(0, 600, 600)
            #plt.scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')


            pplot(x, self.sample_signal ,indexes)


            #plt.show()
            #plt.scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='blue')
            #plt.scatter(array(mintab)[:,0], array(mintab)[:,1], color='red')
            #plt.show()

            return indexes
        except Exception as e:
            return str(e)


    def peakdet(v, delta, x = None):
        """
        Converted from MATLAB script at http://billauer.co.il/peakdet.html

        Returns two arrays

        function [maxtab, mintab]=peakdet(v, delta, x)
        PEAKDET Detect peaks in a vector
        %        [MAXTAB, MINTAB] = PEAKDET(V, DELTA) finds the local
        %        maxima and minima ("peaks") in the vector V.
        %        MAXTAB and MINTAB consists of two columns. Column 1
        %        contains indices in V, and column 2 the found values.
        %
        %        With [MAXTAB, MINTAB] = PEAKDET(V, DELTA, X) the indices
        %        in MAXTAB and MINTAB are replaced with the corresponding
        %        X-values.
        %
        %        A point is considered a maximum peak if it has the maximal
        %        value, and was preceded (to the left) by a value lower by
        %        DELTA.

         Eli Billauer, 3.4.05 (Explicitly not copyrighted).
         This function is released to the public domain; Any use is allowed.
        """
        try:
            maxtab = []
            mintab = []

            if x is None:
                x = arange(len(v))

            v = asarray(v)

            if len(v) != len(x):
                sys.exit('Input vectors v and x must have same length')
            if not isscalar(delta):
                sys.exit('Input argument delta must be a scalar')
            if delta <= 0:
                sys.exit('Input argument delta must be positive')

            mn, mx = Inf, -Inf
            mnpos, mxpos = NaN, NaN

            lookformax = True

            for i in arange(len(v)):
                this = v[i]
                if this > mx:
                    mx = this
                    mxpos = x[i]
                if this < mn:
                    mn = this
                    mnpos = x[i]

                if lookformax:
                    if this < mx-delta:
                        maxtab.append((mxpos, mx))
                        mn = this
                        mnpos = x[i]
                        lookformax = False
                else:
                    if this > mn+delta:
                        mintab.append((mnpos, mn))
                        mx = this
                        mxpos = x[i]
                        lookformax = True

            return array(maxtab) , array(mintab)
        except Exception as e:
            return "peak"

    def gen_signal(self):
        """
        Due to problem with arduino generating sample
        signal to test functionality of rest of platform
        """
        try:
            self.sample_signal = np.sin(x) + np.random.random(600) * 0.001
            x = np.linspace(0, 600, len(self.sample_signal))
            sample_peaks = sp.signal.find_peaks_cwt(self.sample_signal, np.arange(1,3) )
            plt.plot(x, sample_peaks)
            return "sample generated! and peaks detected"
        except Exception as e:
            return str(e)

    def total_peaks(self, peaks):
        try:
            #number of peaks in segment
            tot = len(peaks)
            flag = "total_peaks"
            return tot
        except Exception as e:
            return str(e)

    def mean(self):
        try:
            #calculates average value
            avg = np.mean(self.norm)
            flag = "mean"
            return avg
        except Exception as e:
            return str(e)

    def st_deviation(self):
        try:
            # standard deviation
            stdev = np.std(self.norm)
            flag = "std_dev"
            return stdev
        except Exception as e:
            return str(e)

    def peak_height(self):
        #this method takes in a 2d array of peak-trough sets
        #calculates mean peak heigh OY
        flag = "done"
        return flag

    def rising_time(self):
        #calculates mean OX of the values
        flag = "done"
        return flag

    def cumalitive_ampl(self):
        #sum of OX
        flag = "done"
        return flag

    def cumalitive_startle(self):
        #sum of OY
        flag = "done"
        return flag

    # Database commit of features extracted by the class
    def commit_features(self,totalpeaks,mean,stdev):
        try:
            f = Features(totalpeaks=totalpeaks, mean=mean, stdev=stdev)
            f.save()
            return "success"
        except Exception as e:
            return str(e)

    def writearff(self):
        try:
            data = arff.load(open('/home/dev/adissertation/emofeats.arff', 'rb'))
            obj = {
               'description': 'The features for emorecog',
               'relation': 'stress',
               'attributes': [
                   ('totalpeaks', 'REAL'),
                   ('mean', 'REAL'),
                   ('stdev', 'REAL')
               ],
               'data': Features.objects.all(),
               }
            return "sucess"
        except Exception as e:
            return str(e)
